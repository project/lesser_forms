<?php
/**
 * @file
 * Contains \Drupal\lesser_forms\Controller\LesserFormsController.
 */

namespace Drupal\lesser_forms\Controller;

// New tommy
use \Drupal\Core\Ajax\AjaxResponse;
use \Drupal\Core\Ajax\AppendCommand;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\Role;
use Drupal\node\Entity\NodeType;

/**
 * LesserFormsController.
 */
class LesserFormsController extends ConfigFormBase  {

  public static $configFields = array(
    'promote',
    'sticky',
    'preview',
    'author',
    'revision_information',
    'path',
  );

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $fieldsCount = 0;

  /**
   * Gets the form ID.
   */
  public function getFormId() {
    return 'lesser_forms_settings';
  }

  /**
   * Show the configuration panel.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::config('lesser_forms.settings');
    $applies_to = $config->get('applies_to');

    $user_roles = Role::loadMultiple();
    $header = $this->getTableHeader($user_roles);

    $form['lf_table'] = array(
      '#type' => 'table',
      '#header' => $header,
    );
    foreach (self::$configFields as $field) {
      $form['lf_table'][$field]['title'] = array(
        '#plain_text' => $field,
      );
      $form = $this::printUserRoles($form, $field);
    }
    // Add Custom Fields to table.
    $form['add_custom_field']= array(
      '#type' => 'textfield',
      '#title' => 'Add Custom Field',
    );
    $form['custom_field_submit'] = array(
      '#type' => 'button',
      '#value' => 'Add',
      '#ajax' => array(
        'keypress' => TRUE,
        'callback' => array($this, 'add_custom_field'),
        //'wrapper' => 'lf_table',
        'event' => 'click',
        'effect' => 'fade',
      ),
    );
    // Lesser Forms is active on these form machine names.
    $form['applies_to'] = array(
      '#title' => t('Enabled on'),
      '#type' => 'textarea',
      '#description' => t('Lesser Forms will only be active on the form machine names you insert here. Only one machine name per line. Use new lines to add multiples.') . '<br>' . t('You can use a wildcard "*"-symbol to apply to every standardized form, but keep in mind this is arbitrary.') . '<br>' . t('These content type forms are known by your site already:') . ' ' . $this->getNodeTypesForms(),
      '#default_value' => $applies_to,
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save config'),
    );
    return $form;
  }

  /**
   * Custom Callback to add custom form field to array
   */
  public function printUserRoles($form, $field){
    $user_roles = Role::loadMultiple();
    $config = \Drupal::config('lesser_forms.settings');
    $fields = $config->get('fields');

    foreach ($user_roles as $id => $entity) {
      $form['lf_table'][$field][$entity->id()] = array(
        '#type' => 'checkbox',
        '#default_value' => $this->getSettingsValue($fields, $entity->id(), $field),
      );
    }
    return $form;
  }
  /**
   * Custom Callback to add custom form field to array
   */
  function add_custom_field(array &$form, FormStateInterface $form_state) {
    //var_dump( $this->fieldsCount );
    $field = rand(10,100);
    $fields['lf_table'][$field]['title']= array(
      '#plain_text' => 'kaka',
    );
    $row = $form['lf_table'];
    $row[0]['title'] = array(
      '#plain_text' => $field,
    );
    //$form = $this::printUserRoles($form, $field);
    $response = new AjaxResponse();
    $response->addCommand(new AppendCommand('#edit-lf-table tbody', $row[0]));

    $form_state->setRebuild();

    return $response;
  }

  /**
   * Get the default value of the checkbox out of the settings.
   */
  public function getSettingsValue($settings, $role_name, $field_name) {
    if ($settings == NULL || $settings[$field_name] == NULL) {
      return FALSE;
    }
    return $settings[$field_name][$role_name];
  }

  /**
   * Prepares the config table header.
   */
  public function getTableHeader($user_roles) {
    $header = array(t('field'));

    foreach ($user_roles as $id => $entity) {
      $header[] = $entity->label();
    }
    return $header;
  }

  /**
   * Submit form and save values.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::configFactory()->getEditable('lesser_forms.settings');
    $config->set('fields', $form_state->getUserInput()['lf_table']);
    $config->set('applies_to', $form_state->getUserInput()['applies_to']);
    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Form validation.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Get the editable configuration names.
   *
   * @return array
   */
  public function getEditableConfigNames() {
    return [
      'lesser_forms.settings',
    ];
  }

  public function getNodeTypesForms() {
    $array_keys = array_keys(NodeType::loadMultiple());

    // Format the keys. @todo: Is there a better way of formatting the form_id?
    foreach ($array_keys as $item) {
      $items[$item] = 'node_' . $item . '_form';
      $items[] = 'node_' . $item . '_edit_form';
    }

    return implode(', ', $items);
  }


}
